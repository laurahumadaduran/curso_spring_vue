package com.grupoica.repasojava.abastractas_interfaces;

public interface Animal {

	void alimentarse(String comida);
}
