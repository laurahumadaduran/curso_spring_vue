package com.grupoica.repasojava.abastractas_interfaces;

public class Coche extends Vehiculo implements Motorizable {
	float gasolina;
	
	
	public Coche(String marca, float peso, float gasolina) {
		super(marca, peso);
		// TODO Auto-generated constructor stub
		
	}
	
	
	public float getGasolina() {
		return gasolina;
	}

	public void setGasolina(float gasolina) {
		this.gasolina = gasolina;
	}


	@Override
	public void aceleracion() {
		// TODO Auto-generated method stub
		super.aceleracion();
		System.out.println(marca + " Acelerando coche, cuidado" );
	}


	@Override
	public void desplarse(float distancia) {
		// TODO Auto-generated method stub
		System.out.println(marca + " rueda "+ distancia + " metros");
		
	}
	
	@Override
	public void encender() {
		// TODO Auto-generated method stub
		System.out.println(marca + " enciende ");
		
	}


	

}
