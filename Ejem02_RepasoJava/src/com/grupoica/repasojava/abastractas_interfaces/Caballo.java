package com.grupoica.repasojava.abastractas_interfaces;

public class Caballo extends Vehiculo implements Animal {

	int dientes;
	String color;
	String raza;
	

	public Caballo(String marca, float peso, int dientes, String color, String raza) {
		super(marca, peso);
		this.dientes = dientes;
		this.color = color;
		this.raza = raza;
	}

	public int getDientes() {
		return dientes;
	}

	public void setDientes(int dientes) {
		this.dientes = dientes;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getRaza() {
		return raza;
	}

	public void setRaza(String raza) {
		this.raza = raza;
	}

	
	@Override
	public void aceleracion() {
		// TODO Auto-generated method stub
		super.aceleracion();
		System.out.println(marca + " Acelerando caballo, cuidado" );
	}

	@Override
	public void desplarse(float distancia) {
		// TODO Auto-generated method stub
		System.out.println(marca + " galopa "+ distancia + " metros");
		
	}

	@Override
	public void alimentarse(String comida) {
		// TODO Auto-generated method stub
		System.out.println("Caballo alimentado");
		
	}

	
	
	

}
