package com.grupoica.repasojava.abastractas_interfaces;

import java.util.ArrayList;

public class ProbandoVehiculos {


	
	public static void probar() {
		
		Coche miCoche = new Coche("Kia", 1500, 60.34f);
		miCoche.aceleracion();
		
		Coche miCocheFindes = new Coche("Hammer", 2500, 60.34f);
		
		Caballo miCaballo = new Caballo("Aleman", 80, 30, "negro", "Alemana");
		miCaballo.aceleracion();
		
		//Patinete miPatinete = new Patinete();
		
		//Polimosfirsmo: pero lo que se pasa es la referencia o direccion de memoria
		Vehiculo unVehiculo = miCoche; //Casting implicito
		Object unObjeto = miCoche;
		Coche uncoche = (Coche) unObjeto;//Casting explicito
		
		System.out.println(unObjeto.toString());
		unVehiculo.aceleracion();
		
		ArrayList<Motorizable> garajeCoches = new ArrayList<>();
		//Motorizable garajeCoches = (Motorizable) garaje;
		
		garajeCoches.add(miCoche);
		garajeCoches.add(miCocheFindes);
		
		
		garajeCoches.add(new Patinete(15));
		
		//No se puede porque la clase Vehiculo es abstracta 
		//y no se puede instanciar un objeto de tipo vehiculo
		//garaje.add(new Vehiculo("Que no he comprado",30));
		garajeCoches.add((Coche) unVehiculo);
		
		ArrayList<Animal> granja = new ArrayList<>();
		granja.add(new Perro("GUAU"));
		granja.add(miCaballo);
		
		for (Animal animal : granja){
			animal.alimentarse("comida");
			animal.alimentarse("comida");
		}
		
		
		for (Motorizable objMotor : garajeCoches) {
			if(objMotor instanceof Vehiculo) {
				Vehiculo vehiculo = (Vehiculo) objMotor;
				vehiculo.aceleracion();
				vehiculo.desplarse(12.67f);
				
			}
			
			objMotor.encender();
		}

		miCoche.encender();
		Motorizable vehMotor = miCoche;
		vehMotor.encender();
		
		

		
		
		/* Ejercicios: 
		 * 1 - Garaje ser� solo para objetos motorizables
		 * 2 - Crear clase Patinete que sea motorizable, pero no veh�culo
		 * 3 - Guarderemos un patinete en el garaje 
		 * 4 - Hacer una clase Perro (que tampoco es un veh�culo)
		 * 5 - Crear una interfaz Animal con metodo 
		 * 			alimentarse(String comida)
		 * 6 - Perro y Caballo que sean animales, y hacer una granja
		 * 		y alimentarlos.
		 * */


		
	}

}
