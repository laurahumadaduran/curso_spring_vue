package com.grupoica.repasojava.modelo;

import java.util.ArrayList;
import java.util.function.UnaryOperator;

import com.grupoica.repasojava.EjemploHashMap;

/*
 * Clase que se encargara de las operaciones C.R.U.D.
 * Create Read Update Delete (Op, Alta, baja, modificacion y consulta)
 */
public class GestionUsuarios {
	
	//private ArrayList listaUsuarios;
	//lista en su forma ambigua (todos los elementos son object)
	//private ArrayLit listaUsuarios; es lo mismo que ArrayList<Object>
	//lista en su forma generica
	//(todos los elementos son del mismo o del algun heredero)
	private ArrayList<Usuario> listaUsuarios;
	
	public GestionUsuarios() {
		super();
		this.listaUsuarios = new ArrayList();
		
	}
	
	public void listarUsuarios() {
		for(int i =0; i< this.listaUsuarios.size(); i++) {
			System.out.println(this.listaUsuarios.get(i));
		}
	
	}
	
	public void mostrarUsuario(String nombre) {
		double doble = 10.43434;
		int entero = (int) Math.floor(doble); //Casting (conversion de un tipo de dato a otro)
		
		for(Usuario usu : listaUsuarios) {
			
			//if(usuObj.getClass().equals(Usuario.class))
			//if(usuObj instanceof Usuario) {
				//Usuario usu = (Usuario) usuObj; //Casting 
				if(usu.getNombre().equals(nombre)) {
					System.out.println("ENCONTRADO Nombre: "+ usu.getNombre());
				}else {
					System.out.println("NO ENCONTRADO");
				}
			//}
		}
		
	}
	
	public void modificarNomUsuario(String nombreantiguo, String nomNuevo) {
		for(Usuario usu : listaUsuarios) {
			if(!(usu.getNombre().equalsIgnoreCase(nombreantiguo))) {
				
				System.out.println("USUARIO Modificado de: "+ usu.getNombre()+ " a "+ nombreantiguo);
				usu.setNombre(nomNuevo);
			}
		}
	}
	
	public void modificarEdadUsuario(String nombreantiguo, int edad) {
		for(Usuario usu : listaUsuarios) {
			if(!(usu.getNombre().equalsIgnoreCase(nombreantiguo))) {
				
				System.out.println("USUARIO Modificado a edad: "+ usu.getEdad());
				usu.setEdad(edad);
			}
		}
	}
	
	public void modificarUsuario(String nombreantiguo, String nombre, int edad) {
		for(Usuario usu : listaUsuarios) {
			if(!(usu.getNombre().equalsIgnoreCase(nombreantiguo))) {
				System.out.println("USUARIO Modificado de: "+ usu.getNombre()+ " a "+ nombreantiguo);
				System.out.println("USUARIO Modificado a edad: "+ usu.getEdad());
				usu.setNombre(nombre);
				usu.setEdad(edad);
			}
		}
	}
	
	public void eliminarUsuario(String nombre) {
		for(Usuario usu : listaUsuarios) {
			if(usu.getNombre().equalsIgnoreCase(nombre)) {
				System.out.println("USUARIO ELIMINADO Nombre: "+ usu.getNombre());
				listaUsuarios.remove(usu);
			}
		}
	}
	
	public void eliminarTodosUsuario() {
		for(Usuario usu : listaUsuarios) {
			
				System.out.println("ELIMINADO TODOS USUARIOS");
				listaUsuarios.clear();
			
		}
	}
	
	public void filtrarEdad(int edad) {
		
		
		for(Usuario usu : listaUsuarios) {
			
			
				if(usu.getEdad()==(edad)) {
					System.out.println("Edad: "+ usu.getEdad());
				}else {
					System.out.println("NO ENCONTRADO");
				}
			
		}
		
	}
	
	public void filtrarRangoEdad(int edad, int edad2) {
		
		boolean encontrado = false;
		
		for(Usuario usu : listaUsuarios) {
			
				
				if(usu.getEdad()>(edad) && usu.getEdad()<edad2) {
					encontrado = true;
					System.out.println("RANGO Edad: "+ usu.getEdad());
				}
				if(! encontrado){
					System.out.println("NO ENCONTRADO");
				}
			
		}
		
	}
	
	
	
	//lo que recibe obj es una referencia de memoria poruqe es un objeto y asi trabaja la memoria RAM con los objetos
	public void addUsu(Usuario obj) {
		this.listaUsuarios.add(obj);
		
	}
	
	public void addUsu(String nombre, int edad) {
		Usuario nuevoUsu = new Usuario(nombre,edad);
		this.listaUsuarios.add(nuevoUsu);
		EjemploHashMap.disccUsuarios.put(nuevoUsu.getNombre(), nuevoUsu);
	}
	
}
