package com.grupoica.repasojava.modelo;

public class Loco extends Usuario {
	private boolean tipoLocura;
	
	
	public Loco() {
		//con el super se llama al contructor padre (En este caso Usuario)
		super();
	}

	public boolean isTipoLocura() {
		return tipoLocura;
	}

	public void setTipoLocura(boolean tipoLocura) {
		this.tipoLocura = tipoLocura;
	}

	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		Usuario usuario = (Usuario) obj;
		return this.getNombre() == usuario.getNombre() &&
				this.getEdad() == usuario.getEdad();
	}
	//@Override Sobrecara de metodo (No override, no sobreescritura)
	public boolean equals(Usuario usuario) {
		// TODO Auto-generated method stub
		return this.getNombre() ==usuario.getNombre() &&
				this.getEdad() ==usuario.getEdad();
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "Tipo de Locura "+ tipoLocura;
	}
	
	
	
}
