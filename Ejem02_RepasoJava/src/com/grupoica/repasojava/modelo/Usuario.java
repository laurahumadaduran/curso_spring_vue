package com.grupoica.repasojava.modelo;

//Tipica clase Plain Old Java Object (POJO)
/*
 * POJO es una clase que no extiende ni implementa ni tiene nada especial
 */
public class Usuario /*extends Object (ya esta por defecto esto)*/ {
	
	private String nombre;
	private int edad;
	
	public Usuario() {
		//con el super se llama al constructor padre (que en este caso es object)
		super();
		nombre="Sin nombre";
	}
	
	//sobrecarga de constructores
	public Usuario(String nombre, int edad){
		this.nombre = nombre;
		this.edad = edad;
	
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		Usuario usuario = (Usuario) obj;
		return this.nombre == usuario.nombre &&
				this.edad == usuario.edad;
	}
	//@Override Sobrecarga de metodo (No override, no sobreescritura)
	public boolean equals(Usuario usuario) {
		// TODO Auto-generated method stub
		return this.nombre ==usuario.nombre && this.edad ==usuario.edad;
	}
	
	//con "@" empiezan las anotaciones
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "Usuario "+ nombre + " [ " + edad + " ]";
	}
	
	
	

}
