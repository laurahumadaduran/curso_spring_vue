package com.grupoica.repasojava;

import com.grupoica.repasojava.modelo.Usuario;

public class EjemploMemoria {

	//en las variables estaticas son unicas y globales
	static int xx = 10;
	public static void pruebaPasoPorValor() {
		
		boolean y = true;
		String z = "texto al declarar";
		functionCualquiera(xx,y,z);
		System.out.println("XX = " + xx + " Y " + y  + " Z " + z );
	}
	
	private static void functionCualquiera(int x, boolean y, String z) {
		System.out.println("X = " + x + " Y " + y  + " Z " + z );
		x =200;
		y = false;
		z = "Hola";
		System.out.println("X = " + x + " Y " + y  + " Z " + z );
	}
	
	public static void pruebaPasoPorReferencia() {
		Usuario alguien = new Usuario ("Pepito", 30);
		int array[] = new int[3];
		array[0] = 10; array[1] = 20; array[2] =30;
		
		otraFuncion(alguien, array);
		
		
		System.out.println("1nombre = " + alguien.getNombre() + ", Elemento 0 = " + array[0]);
		int otroArray[] = array;
		otroArray[0] = 333;
		System.out.println("2nombre = "+ alguien.getNombre() + ", Elemento 0 = " + array[0]);
	}
	
	private static void otraFuncion(Usuario parUsu, int[] parArr) {
		System.out.println("3nombre = " + parUsu.getNombre() + ", Elemento 0 = " + parArr[0]);
		
		parUsu.setNombre("Modif en funcion");
		parArr[0] =9999;
		
		System.out.println("4nombre = " + parUsu.getNombre() + ", Elemento 0 = " + parArr[0]);
	}
}

















