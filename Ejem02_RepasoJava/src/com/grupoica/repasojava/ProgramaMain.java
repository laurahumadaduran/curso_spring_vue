package com.grupoica.repasojava;

import java.util.ArrayList;

import com.grupoica.repasojava.abastractas_interfaces.ProbandoVehiculos;
import com.grupoica.repasojava.modelo.GestionUsuarios;
import com.grupoica.repasojava.modelo.Loco;
import com.grupoica.repasojava.modelo.Usuario;

public class ProgramaMain {

	/*
	 * P.O.O
	 * La unidad b�sica de almacenamiento don lod tipos primitivos y los objetos que estan basados en clases. Las clases son el molde, plantilla, estructura que indica
	 * como ser�n todos los objetos instancidos a partir de ella.
	 * Sus variables miembro (campo, atributos, propiedades...) y sus m�todos (funciones propias)
	 * 
	 * -Herencia. La capacidad de las clases para heredar los m�todos y las variables miembro unas de otras. Usando la palabra "extends". Java ya usa la herencia por defecto
	 * -Encapsulacion Capacidad de las clases para limitar el acceso a variables miembro y m�todos. (nivel de acceso private, public, protected (privadi a nivel de herencia) o por defecto (a nivel de paquete))
	 * -Polimorfismo La capacidad de los objetos de obtener la forma de su clase o la de cualquiera de
	 * sus ancestros (padre, abuelo...)
	 * */
	 
	public static void main(String[] args) {
		
		EjemploLambdas.ejecutarLambdas();
		
		//ProbandoVehiculos.probar();
		
		
		/*
		GestionUsuarios gesUsu = new GestionUsuarios();
		
		EjemploHashMap.probandoHashMap();
		
		
		// TODO Auto-generated method stub
		
		//this.listaUsuarios.add(10);
		Usuario usu = new Usuario();
		usu.setEdad(30);
		usu.setNombre("Laura");
		System.out.println("Nombre: "+ usu.getNombre());
		//Si lista
		gesUsu.addUsu(usu);
		
		Usuario u2 = new Usuario("U2", 50);
		System.out.println("Edad u2: "+ u2.getEdad());
		
		//this.listaUsuarios.add(new Usuario());
		//this.listaUsuarios.add("Texto");
		//this.listaUsuarios.add(new Object());
		gesUsu.listarUsuarios();
			
		if (u2.equals(usu)) {
			System.out.println("Son iguales");
		}else {
			System.out.println("Son distintos");
		}
		
		Loco joker = new Loco();
		joker.setNombre("Joker");
		joker.setTipoLocura(true);
		
		if (joker.isTipoLocura()) {
			System.out.println("Esta loco" + joker.toString());
		}else {
			System.out.println("No esta loco"+ joker.toString());
		}
		
		
		System.out.print("Joker: "+ joker.getNombre());
		
		System.out.print("Joker: "+ joker.toString());
		//System.out.println(joker.getNombre().equals(joker.getEdad()));
		gesUsu.addUsu(joker);
		gesUsu.mostrarUsuario("Joker");
		
		
		
		
		
		//gesUsu.addUsu("njc");
		Usuario laura = new Usuario();
		laura.setNombre("Laura");
		laura.setEdad(40);
		
		gesUsu.addUsu(laura);
		
		gesUsu.listarUsuarios();
		
		gesUsu.modificarNomUsuario("Laura", "Maria");
		
		gesUsu.modificarEdadUsuario("Laura",90);
		
		gesUsu.modificarUsuario("Laura", "Maria", 30);
		
		
		
		gesUsu.filtrarEdad(90);
		//gesUsu.listarUsuarios();
		
		gesUsu.filtrarRangoEdad(11,90);
		//gesUsu.mostrarUsuario("Joker");
		
		//gesUsu.eliminarTodosUsuario();
		
		
		EjemploMemoria.pruebaPasoPorValor();
		
		EjemploMemoria.pruebaPasoPorReferencia();*/
		
	}

}
