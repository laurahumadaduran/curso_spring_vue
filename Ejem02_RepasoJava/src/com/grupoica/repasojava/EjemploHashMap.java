package com.grupoica.repasojava;

import java.util.HashMap;
import java.util.Scanner;

import com.grupoica.repasojava.modelo.Usuario;

public class EjemploHashMap {
	
	public static HashMap<String, Usuario> disccUsuarios=new HashMap<>();;
	
	public static void probandoHashMap() {
		disccUsuarios.put("Luis", new Usuario("Luis", 18));
		disccUsuarios.put("Ana", new Usuario("Ana", 20));
		disccUsuarios.put("Luisa", new Usuario("Luisa", 30));
		Scanner escaner = new Scanner(System.in);
		System.out.println("Introduzca usuario");
		String nombre = escaner.nextLine();
		System.out.println("El usuario es "+ disccUsuarios.get(nombre));
	}

}
