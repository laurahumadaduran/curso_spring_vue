Vue.component("app-saludo", {
    "template": `<h2>Hola soy un componente</h2>`
});
Vue.component("app-saludo2", {
    "template": `<h2 class="titulo">Hola soy un componente 2</h2>`,
    style: `.titulo {background-color:red}`
});
new Vue({
    //el es element y con el ·#· llamas al id del div
    //el template es una propiedad para indicarle la plantilla del codigo html
    "el": "#app-section",
    template: `
    <div><h2>Hola ICA</h2> 
    <app-saludo></app-saludo></div>`

});
new Vue({
    el: "#app-section-2"
});

new Vue({
    "el": "#app-section-3",
    template: `
    
    <div><h2>Hola ICA</h2> 
    <app-saludo2></app-saludo2></div>`

});