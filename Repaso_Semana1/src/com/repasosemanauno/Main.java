package com.repasosemanauno;

import com.modelo.Alumno;
import com.modelo.AlumnoRepetidor;
import com.modelo.GestionAlumnos;

public class Main {

	public Main() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Alumno alum = new Alumno();
		
		AlumnoRepetidor alumrepe = new AlumnoRepetidor();
		
		GestionAlumnos gesalum = new GestionAlumnos();
		
		alum.setNombre("Laura");
		alum.setApellido("Ahumada Duran");
		alum.setEdad(20);
		
		alumrepe.setNombre("Manuel");
		alumrepe.setApellido("Garcia Oliva");
		alumrepe.setEdad(23);
		alumrepe.setAsigpend("Programacion");
		
		gesalum.addAlum(alum);
		
		gesalum.addAlum(alumrepe);
		
		gesalum.mostrarDatosAlumnos();

	}

}
