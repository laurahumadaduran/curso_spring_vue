package com.modelo;

import java.util.ArrayList;



public class GestionAlumnos {

	private ArrayList<Alumno> listaAlumnos;
	
	public GestionAlumnos() {
		// TODO Auto-generated constructor stub
			this.listaAlumnos = new ArrayList();
		
	}
	
	
	public void mostrarDatosAlumnos() {
		for(Alumno alum: listaAlumnos) {
			System.out.println("Nombre "+ alum.getNombre() + "\nApellidos: " + alum.getApellido() + "\nEdad: "+ alum.getEdad());
		}
	}
	
	public void addAlum(Alumno alum) {
		this.listaAlumnos.add(alum);
	}

}
