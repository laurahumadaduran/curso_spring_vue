package com.grupoica.sevlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class HolaSevlet
 */
@WebServlet("/Hola")
public class HolaSevlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public HolaSevlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
    	String strnombre =request.getParameter("nombre");
    	String html = "<html> "
    			+ "<head><title>Formulario envio </title></head>"
    			+ "<body>";
    	if(strnombre == null || ("".equals(strnombre))) {
    		html += "<h2>Pon el nombre </h2>";
    	
    	}else {
    		html +=  "<form action='./Hola.do' method='post'>" + 
    				"Veces: <input name='veces' type='number'>" + 
    			"	<input type='submit' value='POST'/> " +
    			"</form>";
			
    	}
	    	html +=  "</body>"
	    			+ "</html>";		
    	response.getWriter().append(html);
		//response.getWriter().append("<h1>Hola desde sevlet </h1> at: ").append(request.getContextPath());
	}
    
    
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String strnombre =request.getParameter("nombre");
    	String html = "<html> "
    			+ "<head><title>Formulario envio </title></head>"
    			+ "<body>";
    	
    	String strveces =request.getParameter("veces");
    	
    	int intveces = Integer.parseInt(strveces);
    	
    	if(strveces == null || ("".equals(strveces))) {
    		html += "<h2>Faltan n� veces </h2>";
    	
    	}else {
	    	for (int i=0; i<intveces; i++) {
	    		html +=i + "<br>";
	    	}
    	}
	    	html +=  "</body>"
	    			+ "</html>";		
    	response.getWriter().append(html);
		//response.getWriter().append("<h1>Hola desde sevlet </h1> at: ").append(request.getContextPath());
	}
	
	

}
