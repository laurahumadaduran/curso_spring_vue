const PI = 3.141596;

let unaVar = 20;
let unTexto = "Hola";

document.write(`</br>
Texto en vrias lineas, y ademas podemos mostrar<br> variables asi: ${unaVar} y otro texto ${unTexto}`);

document.write("<br>");

//Funciones lambda: funciones anonimas
var suma = (x, y) => x + y

document.write("<br>" + suma(3, 5));
var alcuadrado = x => x ** 2;
document.write("<br>" + alcuadrado(3));

class Dato {
    constructor(x, y = 20) {
        this.x = x;
        this.y = y;
    }

    mostrar() {
        document.write(`<br> Dato:x  ${this.x}   y  = ${this.y}`);
    }
}

class Info extends Dato {
    constructor(x, y = 20, z = 20) {
        super(x, y);
        this.z = z;
    }

    mostrar() {
        super.mostrar();
        document.write(`<br> Dato:z  ${this.z}`);
    }
}

let dato = new Dato("Hola");
dato.mostrar();

let info = new Info("Hola 2");
info.mostrar();