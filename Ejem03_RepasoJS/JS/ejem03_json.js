//JSON: JavaScript Object Notation
//Otra forma de crear objetos con la notacion JSON
let objetoVacio = {}; //es lo miso que new Object() es lo mismo

let formaPago = {
    "modo": "Tarjeta credito",
    "comision": 2,
    "activa": true,
    "preparacion": null,
    "clientes": ["Santander", "Sabadel", "BBVA", [1, 23, 55]],
    "configuracion": {
        "conexion": "ssl",
        "latencia": 15
    }
};
let arrayVacio = []; // new Array
let datos = ["Churros", "Meninas", 200, true, null, { "ale": "mas datos" }];

formaPago.servidor = "http://visa.com";

formaPago["oculta"] = "Dame 5 cent";

document.write(`<br>
<p>${formaPago.modo} - ${formaPago.clientes[3][2]} - </p>

<h2>${JSON.stringify(formaPago, null, 3)}</h2>
En el HashMAP esta accediendo a esa posicion con el nombre servidor
Usando forma HashMap: ${formaPago["servidor"] }
`);
alert(JSON.stringify(formaPago, null, 3));

window.localStorage.setItem("datos-forma-pago", JSON.stringify(formaPago, null, 3));
let formaPago = JSON.parse(window.localStorage.getItem("datos-forma-pago", JSON.stringify(formaPago)));

alert(JSON.stringify(formaPago, null, 3));
let petUsu = prompt("¿Que dato quieres ");
document.write(`<br> ${formaPago[petUsu]}`);

let frutas = '[{"nombre": "pera", "precio": 20}, {"nombre": "kiwi", "precio":10}, {"nombre": "fresa", "precio": 15}]';

//pasamos de objeto a texto claro con el parse
let objFrutas = JSON.parse(frutas);
document.write(`<br> ${objFrutas[1].nombre} - ${objFrutas[2]["precio"]}`);